Plan of action and then working notes

### Feature 2: Set up the Django porject apps

Project: Tracker
Apps: acccounts, projects, tasks
set all apps to INSTALLED_APPS,list in tracker\settings

migrate ---> m
create superuser

### Feature 3: The Project model

App: Project
Attributes:
name Charfield max 200
description TextField
members ManyToManyField auth.User
rel name:projects

import AUTH_USER_MODEL from setting
declare auth_USER as setting.AUTH_USER_MODEL

makemigration --->mkm
m

### Feature 4: Registering Project in the admin

register Projects to Django admin
import model
admin.site.register( )

### Feature 5: The Project list view

import class ListView from django.views.generic.list

create a class ProjectListView
create template dir> porjects dir> list.html
create urls.py in projects apps

import path from django.urls

create a path with "" and name it "list_projects"
with ProjectListView.as_view

on tracker url.py
create a path with "projects" with (include("projects.urls"))

create project list view html project

# use {% if not project_list %}

to return "You are not assigned to any porject"

use for loop to grab the list of projects from project_list

### Feature 6: Default path redirect

create a RedirectView on tracker urls.py
so, when user goes to the localhost:8000 they'll be
redirected to /Projects

import RedirectView from django.views.generic.base

dont forget .as_view since RedirectView is a class

inside the view add url = (url=("/projects/"))

name the redirect "home"

### Feature 7: Login page

Create url path for the login
a) create urls.py under accounts dir
b) import path from django.urls
c) import LoginView from django.contrib.auth.views
d) Register LoginView to the path "login/" and name
that path "login"
e) 'include' the url path onto the tracker urls.py. Path is "accounts/"

Create a template
a) make a template dir under accounts
b) make a registration dir under tempalte
c) make a login file called "login.html"

Register LOGIN_REDIRECT_URL under Tracker/setting.py
and name it "home"

### Feature 8: Require login for Project list view

import LoginReuiredMixin from django.contrib.auth.mixins

inherit that mixin on to the ProjectListView. IMPORTANT place it on the left most side.

Change the queryset by making get_queryset function. set member equals the logged in user by
a) getting the Project object
(Project.object.filter <--to filter())
b) inside the filter bracket set memeber=self.request.user

### Feature 9: Logout page

Create url path for the logout
a) create urls.py under accounts dir
b) import path from django.urls
c) import LoginView from django.contrib.auth.views
d) Register LogoutView to the path "logout/" and name
that path "logout"

Register LOGOUT_REDIRECT_URL under Tracker/setting.py and name it "Login"

### Feature 10: Sign up page

Start creating a Form by creating a form.py file under accounts dir.

import UserCreationForm from django.contrib.auth.froms
import User from django.contrib.auth.models

create a class inside the form called "UserRegistrationForm" inherit UserCreationForm into it.

create Meta class (to change the behavior of the model field)
inside it create model as User. And set fields list as "username", "password1", "password2"

---

Create a function view for the sign-up form for the user:

import UserRegistrationForm class that we made in form.py by "from accounts.form"
import authenticate and login from django.contrib.auth

make function named register and inherit request.

if the request method was POST submit the form and get the form data.
then if the form is valid then use the form.cleaned_data to access the clean data.

## (form class is responsible not only for validating data but normalizing it to a consistent format, via cleaning data)

save the form (form.save())
authenticate again and declare that value to the user variable

if user exist then use login function:
takes the HttpRequest object (request) and User object (user)
And redirect to "home page" projectListView

if user was not requested then send a user a form and reqdirect to signup html

---

Create a signup.html under accounts/template/registeration/
with form tag, csrf token, form.as_p

### Feature 11: The Task model

create a Task model (inherit models.Model) in tasks app

import models from django.db
import settings from django.conf

set settings.AUTH_USER_MODEL to auth_USER variable

name CharField max 200

start_date DateTimeField buth auto_now and
auto_now_add set to False

due_date DateTimeField buth auto_now and auto_now_add set to False

is_completed BooleanField default False

project Foreign key "project.Projects" related_name="tasks"
on_delete = models.cascade

assignee Foreign key auth.User related_name= "tasks" on_delte = models.SET_NULL
AND null= True

## SET_NULL allows the tasks to stay even if the user is deleted

have **str** method with name property so they will convert to string.

## use yesno filter for is_completed (which has the BooleanField) and type "Yes,No"

is is_complete was True user will see Yes. If its False it will show No.

### Feature 12: Registering Task in the admin

register Tasks to Django admin
import models
admin.site.register( )

### Feature 13: The Project detail view

import DetailView from django.views.generic.detail

create a class called ProjectDetailView with LoginRequiredMixin and DetailVIew inherited on it.
set model to Project
set "project/detail.html" to template_name

under projects urls import ProjectDetailView
create a path, "<int:pk>/" and name it "show_project"
Also, add ProjectDetailView.as_view() to the path.

---

Create detail.html

add link to the project's name.
{% url "show_project" project.pk %}
"show_project" is name to link to the detail page
project.pk is to get the key of the project from Task model

project.name to access the project's name of that project

project.description to access the project's description of that project

if projecy.tasks.all list exist then shows the table of Name, Assignment, Start date, Due, date, Is completed

else show "This project has no tasks"

project.tasks.all to access all the list of all the task

project.tasks.all filter "|" length to get the length of the list.

### Feature 14: The Project create view

On projects/views.py

Import CreateView from django.views.generic.detail
Import redirect from django.urls

Create ProjectCreateView with LoginRequiredMixin and CreateView as the inheritance

model -> Project
template_name -> "projects/new.html"
fields -> name, description, members

Create a get_success_url method under ProjectCreateView
user reverse function to return to project detail view named = "show_project"

## add kwargs = {'pk':self.object.pk} so that the pk will be replace with <self.object.pk> under the url definition --> Which is "<int:pk>"

https://www.appsloveworld.com/django/100/18/django-reverse-using-kwargs

---

On projects/urls.py

Import ProjectCreateView from projects.views

create a path with "create/" and name it "create_project" include the view ProjectCreateView.as_view()

---

create new.html under template/projects dir

on list.html crate a link just below the h1 tag.
set the url path to "creat_project" so the user will be linked to the create project site.

### Feature 15: The Task create view

Import CreateView from django.views.generic.edit
Import LoginRequiredMixin from django.contrib.auth.mixins
Import Task models from tasks.models

Create a TaskCreateView with LoginRequiredMixin and CreateView inherited

model -> Task
template_name -> "tasks/new.html"
fields -> name, start_date, due_date, project. assignee

create a form_valid method with self and form inherited to it.

a) create a save form that doesnt save yet (commit=false) and declare that to task(name for readability) variable.
b) Assign members to the task
c) Save it to the database
e) redirect to the detail page for the project
with pk = task.project.id (so it goes to correct ID for the project)

---

create urls.py under tasks dir
on tasks.urls.py

Import TaskCreateView from tasks.views

create a path with "create/" and name it "create_task" include the view TaskCreatView.as_view()

on tracker.urls.py

create a path with "tasks/" with include("tasks.urls")

---

Create a templates dir under tasks dir and tasks dir under templates
Create a new.html template under

### Feature 16: Show "My Tasks" list view

import ListView from django.views.generic.list

Create TaskListView inherit LoginRequireMixin and ListView.

model -> Task
template name -> "task/list.html

create a get_queryset method so that they see their own task via:
returning Taks.objects.filter(assignee = self.request.user)

---

on tasks.urls.py
create a path with "mine/" and name it "show_my_tasks"
add TaskListView.as_view

---

Create list.html under template/tasks

use if statment so only the task assigned to the person will be shown. Else, use p tag and write "You have no tasks"
{% if task_list %}

## use yesno filter for is_completed (which has the BooleanField) and type "Done,"

is is_complete was True user will see DONe. If its False it will show none

### Feature 17: Completing a task

import UpdateView from django.views.generic.edit
import reverse_lazy from django.urls

Create TaskUpdateView with LoginRequiredMixin, UpdateView inherited on it

model -> Task
fields -> is_completed
suucess_url -> reverse_lazy("show_my_tasks)

---

on tasks.urls.py
create a path with "<int:pk>/complete/" and name it "complete_task" add TaskUpdateView.as_view

---

on tasks.list.html
inside <td> tag of task.is_completed|yesno:"Done," place a if statement that shows Completed button if they haven not completed the task.

### Feature 18: Markdownify

install django-markdownify by
pip install django-markdownify

put it into the INSTALLED_APPS
in the tracker settings.py
(https://django-markdownify.readthedocs.io/en/latest/install_and_usage.html#installation)

also, disable sanitation by adding the following code inside tracker setting.py
(https://django-markdownify.readthedocs.io/en/latest/settings.html#disable-sanitation-bleach)

loading markdownify on the detail.html of projects by typing {% load markdownify %}
on the very top.

then replace p tag and {{ project.description }} and replace it with {{ project.description|markdownify }}

---

update requirements.txt by
pip freeze > requirements.txt

### Feature 19: Navigation

create a base.html directly under project's templates dir.
delete all html,head,body tag from .html other then base.html
then add {% extends "base.html" %}, {% block content %} and {% end content %}
to all. extends goes on very top of each html and block tag wraps around main tag.

on base.html delete all the content inside body tag.

still on base.html add header tag before the body tag then nav tag, ul tag.
create 5 li tags.

1. My tasks ---> href="{% url "show_my_tasks" %"}
2. My projects ---> href="{% url "list_projects"%}"
3. Logout ---> href="{% url "logout" %}"
4. Login ---> href="{% url "login" %}"
5. Signup ---> href="{% url "signup" %}"
   on top of 1 make a if statement tag to see if the user is authenticated
   on top of 4 make a else statement tag to see if the user isnt
   authenticated
   end if statement on button of 5
